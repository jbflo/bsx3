
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Table from '../../../components/table/Table';
import * as sampleAction from '../../../app/actions/scSample';
import * as globalAction from '../../../app/actions/app';


import './style.css';

class SampleTable extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
    };
  }

  componentDidMount() {
    this.props.handleLoadStateLocalStorage();
    this.loadPlateRowColValue();
  }

  componentDidUpdate() { this.props.handleSaveStateLocalStorage(this.props.Rows); }

  loadPlateRowColValue() {
    this.props.plateGrid.map((grid) => {
      if (grid.name === '1') {
        const rows = [];
        const cols = [];
        for (let col = 1; col <= grid.col; col += 1) {
          cols.push(col);
          // for (let row = 1; row <= grid.row; row += 1) {
          //   // rows.push(`${grid.RowHeader[row]}${col}`);
          //   rows.push(`${grid.RowHeader[row]}$`);
          // }
        }
        for (let row = 0; row < grid.row; row += 1) {
          // rows.push(`${grid.RowHeader[row]}${col}`);
          rows.push(grid.RowHeader[row]);
        }
        return (
          this.props.handleLoadRows(rows),
          this.props.handleLoadColumns(cols)
        );
      }
      return null;
    });
  }

  handleRowChange(event) {
    const key = event.target.name;
    let val = null;
    if (event.target.value === 'true' || event.target.value === 'false') {
      val = event.target.checked;
    } else val = event.target.value;

    if (event.target.name === 'plate') {
      this.handlePlateRowColValue(event.target.value);
    }

    Object.entries(this.props.columns).map(
      ([keys]) => {
        if (key === keys) {
          return this.setState({ [keys]: val });
        }
        return null;
      }
    );
  }

  render() {
    return (
      <div className="sctable">
        <Table {...this.props} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    rows: state.sample.rows,
    columns: state.sample.columns,
    editingRow: state.sample.editingRow,
    isAddingNewRow: state.sample.isAddingNewRow,
    plateGrid: state.sample.plateGrid,
    showNotification: state.app.showNotification,
    bufferRows: state.buffer.rows,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    handleIsAddingNewRow: sampleAction.isAddingNewRowAction,
    handleAddRow: sampleAction.addNewRowAction,
    handleDuplicateRow: sampleAction.duplicateNewRowAction,

    handleSelectEditRow: sampleAction.selectEditRowAction,
    handleEditRow: sampleAction.editRowAction,
    handleCancelEditRow: sampleAction.cancelEditRowAction,

    handleDeleteRow: sampleAction.deleteRowAction,

    handleSaveStateLocalStorage: sampleAction.saveStateLocalStorageAction,
    handleLoadStateLocalStorage: sampleAction.loadStateLocalStorageAction,

    handleReorderRow: sampleAction.reorderRowAction,
    handleColumnChooser: sampleAction.toggleColumnChooserAction,
    handleLoadRows: sampleAction.loadPlateRowsAction,
    handleLoadColumns: sampleAction.loadPlateColumnsAction,
    handleShowNotification: globalAction.showNotificationAction,

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SampleTable);
